﻿////2//
//1///3 - нумерация дочерних кругов
////4//

var ctx,m=6,z=1,nx=250,ny=250;

function RASH(x,y,r,p,c){
//x y - координаты центра, r - радиус, p - положение круга-родителя(смотри выше), c - номер итерации/поколение.	
	var color;

	if(c>m) return;
	
	c++;

	     if(c==1) color='#ADFF2F';
	else if(c==2) color='#32CD32';
	else if(c==3) color='#008000';
	else if(c==4) color='#0000FF';
	else if(c==5) color='#C71585';
	else if(c==6) color='#FF0000';
	else if(c==7) color='#FAF0E6';
	else if(c==8) color='#0000FF';
	
	
	if(p==0){//первый груг
		ctx.beginPath();
		ctx.fillStyle = "#4B0082";
		ctx.arc(x, y, r, 0, 2*Math.PI);
		ctx.fill();
		ctx.closePath();
	}
	//строим новые круги так чтобы они не рисовались друг в друге ...
	//и при этом не рисуем круги за областью видимости(так мы экономим много времени).
	if( (p==1 || p==2 || p==4 || p==0)){//1
	    if ( (x-3*r/2 > -r/2) && ( x-3*r/2 < 500+r/2 ) ){
	      ctx.beginPath();
		  ctx.fillStyle = color;
	      ctx.moveTo(x-3*r/2, y);
		  ctx.arc(x-3*r/2, y, r/2, 0, 2*Math.PI);
		  ctx.fill();
		  ctx.closePath();
		}
		RASH(x-3*r/2,y,r/2,1,c);
	}
	if( (p==1 || p==2 || p==3 || p==0) ){//2
	    if( (y+3*r/2 > -r/2) && ( y+3*r/2 < 500+r/2 ) ){
	      ctx.beginPath();
		  ctx.fillStyle = color;
	      ctx.moveTo(x, y+3*r/2);
		  ctx.arc(x, y+3*r/2, r/2, 0, 2*Math.PI);
		  ctx.fill();
		  ctx.closePath();
		}
		RASH(x, y+3*r/2,r/2,2,c);
	}
	if( (p==2 || p==3 || p==4 || p==0) ){//3
	    if( (x+3*r/2 > -r/2) && ( x+3*r/2 < 500+r/2 ) ){
	      ctx.beginPath();
		  ctx.fillStyle = color;
	      ctx.moveTo(x+3*r/2, y);
		  ctx.arc(x+3*r/2, y, r/2, 0, 2*Math.PI);
		  ctx.fill();
		  ctx.closePath();
		}
		RASH(x+3*r/2, y,r/2,3,c);
	}
	if( (p==1 || p==3 || p==4 || p==0) ){//4
	    if( (y-3*r/2 > -r/2) && ( y-3*r/2 < 500+r/2 ) ){
	      ctx.beginPath();
		  ctx.fillStyle = color;
	      ctx.moveTo(x, y-3*r/2);
		  ctx.arc(x, y-3*r/2, r/2, 0, 2*Math.PI);
		  ctx.fill();
		  ctx.closePath();
		}
		RASH(x, y-3*r/2,r/2,4,c);	
	}
	
}
function point(evt){
	nx+=250-(evt.clientX-15);
	ny+=250-(evt.clientY-15);
	ctx.clearRect(0,0,500,500);
	RASH(nx,ny,z*80,0,0);
}

function run(){
	ctx = document.getElementById("laba").getContext("2d");
	z = document.getElementById('r').value;
    m = document.getElementById('m').value;
	ctx.clearRect(0,0,500,500);
	RASH(250,250,z*80,0,0);
}

